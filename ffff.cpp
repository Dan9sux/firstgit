#include <iostream>
#include <locale.h>

int main()
{
	setlocale(LC_ALL, "Russian");
	int n = 0,
		mod = 0;
	std::cout << "   : ";
	std::cin >> n;
	mod = n % 10;

	if (!std::cin.good())
	{
		std::cerr << ",      !\n";
		return 1;
	}

	if (n < 0)
	{
		std::cerr << ",      !\n";
		return 1;
	}

	if (n >= 11 && n <= 14)
	{
		std::cout << "  " << n << " \n";
		return 0;
	}
	else
	{
		if ((mod == 0) || (mod >= 5 && mod <= 9))
		{
			std::cout << "  " << n << " \n";
			return 0;
		}
		if (mod == 1)
		{
			std::cout << "  " << n << " \n";
			return 0;
		}
		if (mod >= 2 && mod <= 4)
		{
			std::cout << "  " << n << " \n";
			return 0;
		}
	}

}
